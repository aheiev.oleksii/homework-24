let marvelHeroes = [
  {
    name: "Thor",
  },
  {
    name: "Spider Man",
  },
  {
    name: "Deadpool",
  },
];

let dcHeroes = [
  {
    name: "Superman",
  },
  {
    name: "Batman",
  },
];

Array.prototype.heroesRender = function(path) {
  let renderArr = this.map(function(el) {
    return `<tr>
      <td>${el.name}</td>
        <td>
          <img src="images/${path}/${el.name.toLowerCase().replace(" ", "")}.svg">
        </td>
      </tr>`;
  });

  renderArr.unshift(`<thead>
       <tr>
         <th>Name</th>
         <th>Icon</th>
       </tr>
     </thead>`);
  return renderArr;
};

document.write(`<table>
      <tbody> 
        ${dcHeroes.heroesRender("dc")}
      </tbody>
    </table>`);

document.write(`<table>
      <tbody>
        ${marvelHeroes.heroesRender("marvel")}
      </tbody>
    </table>`);
